import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VagaComponentComponent } from './components/vaga-component/vaga-component.component';
import { TicketComponentComponent } from './components/ticket-component/ticket-component.component';
import { ReportComponentComponent } from './components/report-component/report-component.component';
import { OcuparVagaComponentComponent } from './components/ocupar-vaga-component/ocupar-vaga-component.component';
import { PagarTicketComponentComponent } from './components/pagar-ticket-component/pagar-ticket-component.component';


const routes: Routes = [
  { path: 'vagas', component: VagaComponentComponent },
  { path: 'consultarTickets', component: TicketComponentComponent },
  { path: 'relatorios', component: ReportComponentComponent},
  { path: 'ocuparVaga', component: OcuparVagaComponentComponent},
  { path: 'pagarTicket', component: PagarTicketComponentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

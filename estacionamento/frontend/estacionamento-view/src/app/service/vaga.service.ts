import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { API } from '../endpoints/api.enum';
import { Observable } from 'rxjs';
import { Vaga } from '../model/Vaga';

@Injectable({
    providedIn: 'root'
  })

export class VagaService {

    constructor(private http: HttpClient) { }

    private env = environment;

    private urlTotalVagasDisponiveis = `${this.env.url}/${API.TOTAL_VAGAS_DISPONIVEIS}`;
    private urlVagasDisponiveis = `${this.env.url}/${API.VAGAS_DISPONIVEIS}`;
    private urlOcuparVaga = `${this.env.url}/${API.OCUPAR_VAGA}`;

    vagasDisponiveis(): Observable<Vaga[]> {
        return this.http.get<Vaga[]>(this.urlVagasDisponiveis);
    }

    totalVagas(): Observable<number>{
        return this.http.get<number>(this.urlTotalVagasDisponiveis);
    }

    ocuparVaga(vaga: Vaga) {
        return this.http.post<Vaga>(this.urlOcuparVaga, vaga);
    }

}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { API } from '../endpoints/api.enum';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })

export class ReportService {

    constructor(private http: HttpClient) { }

    private env = environment;

    private urlGerarRelatorio = `${this.env.url}/${API.GERAR_RELATORIO}`;
}
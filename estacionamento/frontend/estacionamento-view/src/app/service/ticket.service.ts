import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { API } from '../endpoints/api.enum';
import { Observable } from 'rxjs';
import { Ticket } from '../model/Ticket';

@Injectable({
    providedIn: 'root'
  })

export class TicketService {

    constructor(private http: HttpClient) { }

    private env = environment;

    private urlValorTicket = `${this.env.url}/${API.VALOR_TICKET}`;
    private urlPagarTicket = `${this.env.url}/${API.PAGAR_TICKET}`;

    consultarTicket(ticket: Ticket){
      return this.http.post<Ticket>(this.urlValorTicket, ticket);
    }

    pagarTicket(ticket: Ticket){
      return this.http.post<Ticket>(this.urlPagarTicket, ticket);
    }
}
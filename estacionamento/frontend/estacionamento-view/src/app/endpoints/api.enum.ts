export enum API {
    VAGAS_DISPONIVEIS = 'vagas/vagasDisponiveis',
    TOTAL_VAGAS_DISPONIVEIS = 'vagas/totalVagasDisponiveis',
    OCUPAR_VAGA = 'vagas/ocuparVaga',
    GERAR_RELATORIO = 'relatorio/gerar',
    VALOR_TICKET = 'ticket/valorPagamento',
    PAGAR_TICKET = 'ticket/pagarTicket'
}
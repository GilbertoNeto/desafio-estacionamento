import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VagaComponentComponent } from './components/vaga-component/vaga-component.component';
import { TicketComponentComponent } from './components/ticket-component/ticket-component.component';
import { ReportComponentComponent } from './components/report-component/report-component.component';
import { OcuparVagaComponentComponent } from './components/ocupar-vaga-component/ocupar-vaga-component.component';
import { PagarTicketComponentComponent } from './components/pagar-ticket-component/pagar-ticket-component.component';
import { HttpClientModule } from '@angular/common/http';
import { VagaService } from './service/vaga.service';
import { TicketService } from './service/ticket.service';
import { ReportService } from './service/report.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    VagaComponentComponent,
    TicketComponentComponent,
    ReportComponentComponent,
    OcuparVagaComponentComponent,
    PagarTicketComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    VagaService,
    TicketService,
    ReportService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

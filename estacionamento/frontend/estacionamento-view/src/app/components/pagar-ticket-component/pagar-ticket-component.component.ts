import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/model/Ticket';
import { ActivatedRoute, Router } from '@angular/router';
import { TicketService } from 'src/app/service/ticket.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pagar-ticket-component',
  templateUrl: './pagar-ticket-component.component.html',
  styleUrls: ['./pagar-ticket-component.component.css']
})
export class PagarTicketComponentComponent implements OnInit {

  ticket: Ticket;

  constructor(private route: ActivatedRoute, private router: Router,
              private ticketService: TicketService) {
                this.ticket = new Ticket();
              }

  ngOnInit() {
  }

  onSubmit() {
    this.ticketService.pagarTicket(this.ticket).subscribe(result => {
      this.ticket = result;
    }, err => {
      console.log(err);
      Swal.fire({
       title: 'Erro :(',
        text: err.error.message,
        type: 'error'
        });
    });
}

}

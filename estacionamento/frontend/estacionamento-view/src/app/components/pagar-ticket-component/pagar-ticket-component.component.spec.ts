import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagarTicketComponentComponent } from './pagar-ticket-component.component';

describe('PagarTicketComponentComponent', () => {
  let component: PagarTicketComponentComponent;
  let fixture: ComponentFixture<PagarTicketComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagarTicketComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagarTicketComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OcuparVagaComponentComponent } from './ocupar-vaga-component.component';

describe('OcuparVagaComponentComponent', () => {
  let component: OcuparVagaComponentComponent;
  let fixture: ComponentFixture<OcuparVagaComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OcuparVagaComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OcuparVagaComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { VagaService } from 'src/app/service/vaga.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Vaga } from 'src/app/model/Vaga';
import { Carro } from 'src/app/model/Carro';

@Component({
  selector: 'app-ocupar-vaga-component',
  templateUrl: './ocupar-vaga-component.component.html',
  styleUrls: ['./ocupar-vaga-component.component.css']
})
export class OcuparVagaComponentComponent implements OnInit {

  vaga: Vaga;
  carro: Carro;
  ticketCode: string;

  constructor(private route: ActivatedRoute, private router: Router,
              private vagaService: VagaService) {
                this.vaga = new Vaga();
                this.carro = new Carro();
              }

  ngOnInit() {
    console.log(this.vaga);
    console.log(this.carro);
  }

  onSubmit() {
      this.vaga.carro = this.carro;
      this.vagaService.ocuparVaga(this.vaga).subscribe(result => this.ticketCode = result.ticket);
  }

  // gotoTicket() {
  //   this.router.navigate(['/consultarTickets']);
  // }


}

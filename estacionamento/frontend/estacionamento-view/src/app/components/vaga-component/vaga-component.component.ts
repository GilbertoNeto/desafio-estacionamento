import { Component, OnInit } from '@angular/core';
import { Vaga } from 'src/app/model/Vaga';
import { VagaService } from 'src/app/service/vaga.service';

@Component({
  selector: 'app-vaga-component',
  templateUrl: './vaga-component.component.html',
  styleUrls: ['./vaga-component.component.css']
})
export class VagaComponentComponent implements OnInit {

  constructor(private vagaService: VagaService) { }

  vagas: Vaga[];

  ngOnInit() {
    this.vagaService.vagasDisponiveis().subscribe(data => {
      this.vagas = data;
    });
  }

}

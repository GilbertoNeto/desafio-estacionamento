import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TicketService } from 'src/app/service/ticket.service';
import { Ticket } from 'src/app/model/Ticket';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ticket-component',
  templateUrl: './ticket-component.component.html',
  styleUrls: ['./ticket-component.component.css']
})
export class TicketComponentComponent implements OnInit {

  ticket: Ticket;

  constructor(private route: ActivatedRoute, private router: Router,
              private ticketService: TicketService) {
                this.ticket = new Ticket();
              }

  ngOnInit() {
  }

  onSubmit() {
    this.ticketService.consultarTicket(this.ticket).subscribe(result => {
      this.ticket = result;
    }, err => {
      console.log(err);
      Swal.fire({
       title: 'Erro :(',
        text: err.error.message,
        type: 'error'
        });
    });
}

}

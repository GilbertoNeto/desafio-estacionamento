import { Vaga } from './Vaga';

export class Ticket {
    id: number;
    vaga: Vaga;
    horarioChegada: string;
    horarioSaida: string;
    pago: boolean;
    valorPagamento: number;
    valorPago: number;
	tempoPermancia: any;
}
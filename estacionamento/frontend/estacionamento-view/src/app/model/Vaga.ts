import { Carro } from './Carro';

export class Vaga {
    carro: Carro;
    id: number;
    ocupada: boolean;
    ticket: string;

}

package br.com.conpay.desafio.estacionamento.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class VagaDTO {

	private CarroDTO carro;
	
	private Long id;
	
	private boolean ocupada;
	
}

package br.com.conpay.desafio.estacionamento.dto;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class TicketDTO {

	private Long id;
	
	private String horarioChegada;
	
	private String horarioSaida;
	
	private Map<String,Long> tempoPermanencia = new HashMap<>();
	
	private boolean pago;
	
	private double valorPagamento;
	
	private double valorPago;
	
	private VagaDTO vagaDTO;

	public void converterLocalDateTimeToString (LocalDateTime dataChegada, LocalDateTime dataSaida) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		
		this.horarioChegada = dataChegada.format(formatter);
		
		this.horarioSaida = dataSaida.format(formatter);

	}
	
	public LocalDateTime converterStringToLocalDateTime(String horario) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
	    return LocalDateTime.parse(horario, formatter);
	    
	}

	
}

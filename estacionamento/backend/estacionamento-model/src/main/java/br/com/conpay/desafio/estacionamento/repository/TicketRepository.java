package br.com.conpay.desafio.estacionamento.repository;

import org.springframework.data.jpa.repository.Query;

import br.com.conpay.desafio.estacionamento.entity.Ticket;

public interface TicketRepository extends GenericCrudRepository<Ticket, Long> {
	
	@Query("SELECT SUM(t.valorPagamento) FROM Ticket t where t.pago = true")
	public Double valorTotalDePagamentos();	

}

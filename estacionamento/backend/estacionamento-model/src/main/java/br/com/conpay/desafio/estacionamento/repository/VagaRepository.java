package br.com.conpay.desafio.estacionamento.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import br.com.conpay.desafio.estacionamento.entity.Vaga;

public interface VagaRepository extends GenericCrudRepository<Vaga, Long> {

	@Query("SELECT v FROM Vaga v WHERE v.ocupada = false")
	public List<Vaga> vagasDisponiveis();
}

package br.com.conpay.desafio.estacionamento.repository;

import org.springframework.data.jpa.repository.Query;

import br.com.conpay.desafio.estacionamento.entity.Carro;

public interface CarroRepository extends GenericCrudRepository<Carro, Long> {

	@Query("SELECT COUNT(c) FROM Carro c")
	public Double totalCarros();
	
}

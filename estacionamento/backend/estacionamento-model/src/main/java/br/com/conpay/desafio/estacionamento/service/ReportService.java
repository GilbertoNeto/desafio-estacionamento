package br.com.conpay.desafio.estacionamento.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conpay.desafio.estacionamento.dto.ReportDTO;


@Service
public class ReportService {
	
	@Autowired
	private CarroService carroService;
	
	@Autowired
	private TicketService ticketService;
	
	ReportDTO relatorioDTO = new ReportDTO();
	
	public ReportDTO obterValoresParaRelatorio() {
		
		relatorioDTO.setTitulo("Relatório Gerencial");
		relatorioDTO.getValoresRelatorioMap().put("Carros", carroService.selecionarTotalDeCarros());
		relatorioDTO.getValoresRelatorioMap().put("Total de Pagamentos", ticketService.totalPagamentos());
		
		return relatorioDTO;
	}
	
}

package br.com.conpay.desafio.estacionamento.service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.glassfish.jersey.internal.guava.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.conpay.desafio.estacionamento.entity.GenericEntity;
import br.com.conpay.desafio.estacionamento.repository.GenericCrudRepository;

public abstract class GenericCrudService<T extends GenericEntity<I>, I, R extends GenericCrudRepository<T, I>> {

	@Autowired
	protected R repository;
	
	private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(GenericCrudService.class);


	@Transactional(rollbackFor = Throwable.class)
	public T salvar(T entidade) throws Exception {
		entidade.setPersisted(existe(entidade.getId()));
		ajustar(entidade);
		entidade = repository.save(entidade);
		return entidade;

	}

	public Long contar() {
		return repository.count();
	}

	public List<T> listar() {
		return Lists.newArrayList(repository.findAll());
	}

	public T buscar(I id) throws Exception {
		Optional<T> retorno = repository.findById(id);
		if (retorno != null && retorno.isPresent()) {
			return retorno.get();
		}
		return null;
	}

	public boolean existe(I id) {
		return !Objects.isNull(id) && repository.existsById(id);
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remover(T entidade) throws Exception {
		try {
			repository.delete(entidade);
		} catch (Exception e) {
			throw new Exception("Não foi possível excluir o registro.", e);
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void remover(I id) throws Exception {
		try {
			repository.deleteById(id);
		} catch (Exception e) {
			throw new Exception("Não foi possível excluir o registro.", e);
		}
	}

	@Transactional(rollbackFor = Throwable.class)
	public void removerTodos() {
		repository.deleteAll();
	}

	private void ajustar(T entidade) {
		if (entidade == null) {
			return;
		}

		try {
			Field[] campos = entidade.getClass().getDeclaredFields();

			for (Field f : campos) {
				f.setAccessible(true);
				Object object = f.get(entidade);
				if (object instanceof CharSequence) {
					object.toString().trim();
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}

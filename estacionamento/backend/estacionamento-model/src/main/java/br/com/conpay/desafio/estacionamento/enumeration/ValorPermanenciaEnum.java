package br.com.conpay.desafio.estacionamento.enumeration;

import lombok.Getter;

@Getter
public enum ValorPermanenciaEnum {

	TEMPO_MINIMO(1, "Até 3 Horas", 7.00),
	HORA_EXTRA(2, "Hora Extra", 3.00);
	
	private Integer id;
	private String descricao;
	private Double valor;
	
	private ValorPermanenciaEnum(Integer id, String descricao, Double valor) {
		this.id = id;
		this.descricao = descricao;
		this.valor = valor;
	}
	
	public static ValorPermanenciaEnum carregarPorId(Integer id) {
		for(ValorPermanenciaEnum valorPermanencia : ValorPermanenciaEnum.values()) {
			if(valorPermanencia.id.equals(id)) {
				return valorPermanencia;
			}
		}
		return null;
	}
	
}

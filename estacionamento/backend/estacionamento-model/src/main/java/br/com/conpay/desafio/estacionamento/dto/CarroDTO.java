package br.com.conpay.desafio.estacionamento.dto;



import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class CarroDTO {
	
	private Long id;
	
	private String placa;
	
	private String tipo;
	
	
}

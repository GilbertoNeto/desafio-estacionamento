package br.com.conpay.desafio.estacionamento.service;

import java.time.LocalDateTime;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conpay.desafio.estacionamento.dto.CarroDTO;
import br.com.conpay.desafio.estacionamento.dto.TicketDTO;
import br.com.conpay.desafio.estacionamento.dto.VagaDTO;
import br.com.conpay.desafio.estacionamento.entity.Carro;
import br.com.conpay.desafio.estacionamento.entity.Ticket;
import br.com.conpay.desafio.estacionamento.entity.Vaga;
import br.com.conpay.desafio.estacionamento.repository.TicketRepository;

@Service
public class TicketService extends GenericCrudService<Ticket, Long, TicketRepository> {

	@Autowired
	private VagaService vagaService;

	public Ticket gerarTicket(Vaga vagaUtilizada) throws Exception {
		Vaga vaga = vagaService.buscar(vagaUtilizada.getId());

		Ticket ticket = new Ticket();

		if (Objects.nonNull(vaga)) {
			ticket.setVaga(vaga);
			ticket.setHoraChegada(LocalDateTime.now());
			ticket.setPago(false);
			salvar(ticket);
		}

		return ticket;
	}

	public TicketDTO solicitarValorPagamento(TicketDTO ticket) throws Exception {
		Ticket ticketSolicitacao = buscar(ticket.getId());

		if (Objects.nonNull(ticketSolicitacao)) {
			ticketSolicitacao.setHoraSaida(ticket.converterStringToLocalDateTime(ticket.getHorarioSaida()));

			ticketSolicitacao.tempoPermanencia(ticketSolicitacao.getHoraChegada(), ticketSolicitacao.getHoraSaida());

			ticketSolicitacao.calcularValorPagamento();

			salvar(ticketSolicitacao);

		}

		ticket.converterLocalDateTimeToString(ticketSolicitacao.getHoraChegada(), ticketSolicitacao.getHoraSaida());

		ticket.setValorPagamento(ticketSolicitacao.getValorPagamento());
		
		Vaga vaga = vagaService.buscar(ticketSolicitacao.getVaga().getId());
		
		ticket.setVagaDTO(new VagaDTO());
		
		BeanUtils.copyProperties(vaga, ticket.getVagaDTO());
		
		Carro carro = vaga.getCarro();
		
		CarroDTO carroDTO = new CarroDTO();
		
		ticket.getVagaDTO().setCarro(carroDTO);
		
		BeanUtils.copyProperties(carro, ticket.getVagaDTO().getCarro());

		return ticket;
	}
	
	public TicketDTO realizarPagamento(TicketDTO ticket) throws Exception {
		
		Ticket ticketPagamento = buscar(ticket.getId());
		
		if(Objects.nonNull(ticketPagamento) && Double.valueOf(ticket.getValorPago()).equals(ticketPagamento.getValorPagamento()) 
				&& !ticketPagamento.isPago()) {
			
			ticketPagamento.setPago(true);
			ticket.setPago(true);
			salvar(ticketPagamento);
			
			Vaga vaga = vagaService.buscar(ticketPagamento.getVaga().getId());
			vaga.setCarro(null);
			vaga.setOcupada(false);
			vagaService.salvar(vaga);
			
			ticket.setVagaDTO(new VagaDTO());
			
			BeanUtils.copyProperties(vaga, ticket.getVagaDTO());
		}
		
		return ticket;
	}
	
	public Double totalPagamentos() {
		return this.repository.valorTotalDePagamentos();
	}
		

}

package br.com.conpay.desafio.estacionamento.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conpay.desafio.estacionamento.dto.VagaDTO;
import br.com.conpay.desafio.estacionamento.entity.Carro;
import br.com.conpay.desafio.estacionamento.entity.Ticket;
import br.com.conpay.desafio.estacionamento.entity.Vaga;
import br.com.conpay.desafio.estacionamento.repository.VagaRepository;

@Service
public class VagaService extends GenericCrudService<Vaga, Long, VagaRepository> {

	@Autowired
	TicketService ticketService;
	
	@Autowired
	CarroService carroService;
	
	private static int vagas = 20;

	List<VagaDTO> vagasDTO = new ArrayList<>();
	
	
	public List<VagaDTO> vagasDisponiveis() {
		List<Vaga> vagas = this.repository.vagasDisponiveis();

		if (Objects.nonNull(vagas)) {
			for (Vaga vaga : vagas) {
				VagaDTO vagaDTOAux = new VagaDTO();
				BeanUtils.copyProperties(vaga, vagaDTOAux);
				vagasDTO.add(vagaDTOAux);	
			}
		}

		return vagasDTO;
	}

	public Vaga ocuparVaga(VagaDTO vaga) throws Exception {

		Vaga vagaAtualizada = this.buscar(vaga.getId());

		if (Objects.nonNull(vagaAtualizada) && Objects.equals(vagaAtualizada.isOcupada(), false)) {
			Carro carro = new Carro();

			BeanUtils.copyProperties(vaga.getCarro(), carro);
			
			carroService.salvar(carro);
			
			vagaAtualizada.setCarro(carro);

			vagaAtualizada.setOcupada(true);

			salvar(vagaAtualizada);

			Ticket ticket = ticketService.gerarTicket(vagaAtualizada);
			
			vagaAtualizada.setTicket(ticket.getId().toString());
		}

		return vagaAtualizada;

	}

}

package br.com.conpay.desafio.estacionamento.service;

import org.springframework.stereotype.Service;

import br.com.conpay.desafio.estacionamento.entity.Carro;
import br.com.conpay.desafio.estacionamento.repository.CarroRepository;

@Service
public class CarroService extends GenericCrudService<Carro, Long, CarroRepository> {
	
	
	public Double selecionarTotalDeCarros() {
	 	return this.repository.totalCarros();
	}
}

package br.com.conpay.desafio.estacionamento.dto;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ReportDTO {
	
	private String titulo;

	private Map<String,Double> valoresRelatorioMap = new HashMap<>();
}

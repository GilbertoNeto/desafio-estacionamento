package br.com.conpay.desafio.estacionamento.entity;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.conpay.desafio.estacionamento.enumeration.ValorPermanenciaEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "ticket")
public class Ticket extends GenericEntity<Long> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ticket_id")
	private Long id;

	@NotNull
	@OneToOne
	@JoinColumn(name = "vaga_id")
	private Vaga vaga;

	@NotNull
	@Column(name = "valor")
	private double valorPagamento;

	@NotNull
	@Column(name = "pago")
	private boolean pago;

	@Column(name = "hora_chegada")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso = ISO.DATE_TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime horaChegada;

	@Column(name = "hora_saida")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm", iso = ISO.DATE_TIME)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
	private LocalDateTime horaSaida;
	
	@Transient
	private Map<String, Long> tempoPermanencia = new HashMap<>();

	public void tempoPermanencia(LocalDateTime horaChegada, LocalDateTime horaSaida) {

		long diffInHours = ChronoUnit.HOURS.between(horaChegada, horaSaida);

		tempoPermanencia.put("Horas", diffInHours);
	}
	
	public void calcularValorPagamento() {
		
		Long permanencia = tempoPermanencia.get("Horas");
		
		if(permanencia.longValue() <= 3) {
			valorPagamento = ValorPermanenciaEnum.TEMPO_MINIMO.getValor();
		} else {
			Long horasExtras = permanencia - 3;
			valorPagamento = ValorPermanenciaEnum.TEMPO_MINIMO.getValor() 
					+ (horasExtras * ValorPermanenciaEnum.HORA_EXTRA.getValor());
		}
		
	}

}

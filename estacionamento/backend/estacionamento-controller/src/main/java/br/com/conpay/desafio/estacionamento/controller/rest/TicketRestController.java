package br.com.conpay.desafio.estacionamento.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.conpay.desafio.estacionamento.dto.TicketDTO;
import br.com.conpay.desafio.estacionamento.entity.Ticket;
import br.com.conpay.desafio.estacionamento.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/ticket", produces = { "application/json" })
@Api(value = "ticket")
public class TicketRestController extends GenericCrudRestController<Ticket, Long, TicketService> {
	
	@Autowired
	TicketService ticketService;
	
	@ApiOperation(value = "Solicita o valor a ser pago.", nickname = "solicitação ticket", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Exception") })
	@PostMapping(value = "/valorPagamento")
	public @ResponseBody ResponseEntity<TicketDTO> solicitarValorTicket(@RequestBody TicketDTO ticket) throws Exception{
		return ResponseEntity.ok(ticketService.solicitarValorPagamento(ticket));
	}
	
	@ApiOperation(value = "Realiza pagamento do Ticket.", nickname = "pagamento ticket", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Exception") })
	@PostMapping(value = "/pagarTicket")
	public @ResponseBody ResponseEntity<TicketDTO> pagarTicket(@RequestBody TicketDTO ticket) throws Exception{
		return ResponseEntity.ok(ticketService.realizarPagamento(ticket));
	}
	
}

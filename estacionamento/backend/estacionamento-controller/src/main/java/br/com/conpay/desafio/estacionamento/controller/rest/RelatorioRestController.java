package br.com.conpay.desafio.estacionamento.controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conpay.desafio.estacionamento.dto.ReportDTO;
import br.com.conpay.desafio.estacionamento.service.ReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/relatorio", produces = { "application/json" })
@Api(value = "relatorio")
public class RelatorioRestController {

	@Autowired
	ReportService reportService;
	
	@ApiOperation(value = "Solicita o relatorio gerencial de carros e pagamentos.", nickname = "solicitação relatorio", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Exception") })
	@GetMapping(value = "/gerar")
	public ResponseEntity<ReportDTO> gerarRelatorio(){
		return ResponseEntity.ok(reportService.obterValoresParaRelatorio());
	}
	
}

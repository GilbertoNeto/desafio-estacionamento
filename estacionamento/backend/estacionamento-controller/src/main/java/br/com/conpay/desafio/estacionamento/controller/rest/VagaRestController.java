package br.com.conpay.desafio.estacionamento.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.conpay.desafio.estacionamento.dto.VagaDTO;
import br.com.conpay.desafio.estacionamento.entity.Vaga;
import br.com.conpay.desafio.estacionamento.service.VagaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/vagas", produces = { "application/json" })
@Api(value = "vagas")
public class VagaRestController extends GenericCrudRestController<Vaga, Long, VagaService> {

	@Autowired
	public VagaService vagaService;
	
	@ApiOperation(value = "Ocupa vaga no estacionamento.", nickname = "ocupar vaga", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Exception") })
	@PostMapping("/ocuparVaga")
	public @ResponseBody ResponseEntity<Vaga> ocuparVaga(@RequestBody VagaDTO vaga) throws Exception{
		return ResponseEntity.ok(vagaService.ocuparVaga(vaga));
	}
	
	@ApiOperation(value = "Solicita o total de vagas disponíveis no estacionamento.", nickname = "solicitação total vagas disponiveis", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Exception") })
	@GetMapping("/totalVagasDisponiveis")
	public ResponseEntity<Integer> totalVagasDisponiveis(){
		return ResponseEntity.ok(vagaService.vagasDisponiveis().size());
	}
	
	@ApiOperation(value = "Solicita as vagas disponveis no estacionamento.", nickname = "solicitação de vagas disponiveis", notes = "")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "ID inválido"),
			@ApiResponse(code = 404, message = "Entidade não encontrada"),
			@ApiResponse(code = 405, message = "Exception") })
	@GetMapping("/vagasDisponiveis")
	public List<VagaDTO> vagasDisponiveis(){
		return vagaService.vagasDisponiveis();
	}
	
}

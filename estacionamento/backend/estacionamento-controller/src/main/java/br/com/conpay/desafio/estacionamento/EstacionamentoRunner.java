package br.com.conpay.desafio.estacionamento;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import br.com.conpay.desafio.estacionamento.entity.Vaga;
import br.com.conpay.desafio.estacionamento.repository.VagaRepository;

@Component
public class EstacionamentoRunner implements CommandLineRunner{
	
	private static final Logger logger = LoggerFactory.getLogger(EstacionamentoRunner.class);
	
	@Autowired
	VagaRepository vagaRepository;
	
	private static int vagas = 20;
	
	 @Override
	 public void run(String... args) throws Exception {
		 int quantidadeVagasCriadas = 0;
			
			List<Vaga> vagasCriadas = new ArrayList<>();
			
			do {
				Vaga vaga = new Vaga();
				vaga.setOcupada(false);
				vagaRepository.save(vaga);
				vagasCriadas.add(vaga);
				quantidadeVagasCriadas++;
			} while (quantidadeVagasCriadas <= vagas);
			
			vagaRepository.findAll().forEach((vaga) -> {
	            logger.info("{}", vaga);
	        });
	 }
}

package br.com.conpay.desafio.estacionamento.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-08-16T15:16:31.320Z")
@Configuration
public class SwaggerDocumentationConfig {

	@Bean
	  public Docket greetingApi() {
	    return new Docket(DocumentationType.SWAGGER_2)
	        .select()
	        .apis(RequestHandlerSelectors.basePackage("br.com.conpay.desafio.estacionamento"))
	        .build()
	        .apiInfo(metaData());

	  }

	  private ApiInfo metaData() {
		  return new ApiInfoBuilder()
		            .title("Conpay - Desafio Estacionamento")
		            .description("Serviços disponíveis")
		            .termsOfServiceUrl("")
		            .version("1.0.0")
		            .build();
	  }

}

package br.com.conpay.desafio.estacionamento.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conpay.desafio.estacionamento.entity.Carro;
import br.com.conpay.desafio.estacionamento.service.CarroService;
import io.swagger.annotations.Api;

@RestController
@RequestMapping(path = "/carro", produces = { "application/json" })
@Api(value = "carro")
public class CarroRestController extends GenericCrudRestController<Carro, Long, CarroService> {

}

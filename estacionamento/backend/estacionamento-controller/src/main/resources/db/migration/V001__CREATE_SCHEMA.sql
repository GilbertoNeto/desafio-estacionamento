CREATE TABLE `carro` (
  `car_id` bigint(20) NOT NULL,
  `placa` varchar(255) NOT NULL,
  `tipo_carro` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `vaga` (
  `vaga_id` bigint(20) NOT NULL,
  `ocupada` bit(1) NOT NULL,
  `car_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`vaga_id`),
  KEY `FK2es2931nt9mkuw4xhrdjfege6` (`car_id`),
  CONSTRAINT `FK2es2931nt9mkuw4xhrdjfege6` FOREIGN KEY (`car_id`) REFERENCES `carro` (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ticket` (
  `ticket_id` bigint(20) NOT NULL,
  `hora_chegada` datetime DEFAULT NULL,
  `hora_saida` datetime DEFAULT NULL,
  `pago` bit(1) NOT NULL,
  `valor` double NOT NULL,
  `vaga_id` bigint(20) NOT NULL,
  PRIMARY KEY (`ticket_id`),
  KEY `FKqxl4y97ik4n3e60auh7f03s4c` (`vaga_id`),
  CONSTRAINT `FKqxl4y97ik4n3e60auh7f03s4c` FOREIGN KEY (`vaga_id`) REFERENCES `vaga` (`vaga_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
